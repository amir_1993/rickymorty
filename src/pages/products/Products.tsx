import React, { useEffect, useRef, useState } from "react";
import { useQuery, gql } from "@apollo/client";
import RMCard from "../../components/R&MCard/R&MCard";
import classes from "./products.module.scss";
import { useDispatch } from "react-redux";
import { RAndMActions } from "../../store/products/products";
import Pagination from "../../components/pagination/pagination";

function Products() {
  const dispatch = useDispatch();
  const countRef = useRef(null);
  const [page, setPage] = useState(1);

  const RIKYMORTY_QUERY = gql`
  {
    characters(page:${page} ) {
      info {
        pages,
      }
      results {
        name
        id
        status
        species
        type
        image
        created
        gender
      }
    }
  }
`;
  const { data, error } = useQuery(RIKYMORTY_QUERY);

  //just for show redux usage else we should pass data as props
  useEffect(() => {
    if (data) {
      dispatch(RAndMActions.setRAndMSlice(data.characters));
      countRef.current = data?.characters?.info.pages;
    }
  }, [data, dispatch]);

  if (error) return <pre>{error.message}</pre>;

  return (
    <div className={classes.productsContainer}>
      <RMCard />
      <Pagination
        count={data?.characters?.info.pages ?? countRef.current}
        currentPage={(page) => setPage(page)}
      />
    </div>
  );
}

export default Products;
