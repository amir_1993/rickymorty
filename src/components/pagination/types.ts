export interface countProps {
  count: number;
  currentPage: (page: number) => void;
}
