import { countProps } from "./types";
import classes from "./pagination.module.scss";
import { memo, useEffect, useState } from "react";

const Pagination = ({ count, currentPage }: countProps) => {
  const [page, setPage] = useState(1);
  const onPageClicked = (page: number) => {
    setPage(page);
  };
  const nextBtn = () => {
    setPage((prev) => (prev < count ? prev + 1 : prev));
  };
  const prevBtn = () => {
    setPage((prev) => (prev > 1 ? prev - 1 : prev));
  };
  useEffect(() => {
    currentPage(page);
  }, [page]);

  if (!count) return <></>;
  return (
    <div className={classes.paginationContainer}>
      <button className={classes.prevBtn} onClick={prevBtn}>{`<`}</button>
      <div className={classes.pages}>
        {Array(count > 4 ? 5 : count)
          .fill("")
          .map((_, index) => (
            <button
              className={page === index + 1 ? classes.hoverBtn : ""}
              key={index}
              onClick={() => onPageClicked(index + 1)}
              style={{
                display: page > 4 && index >= 3 ? "none" : "unset",
              }}
            >
              {index + 1}
            </button>
          ))}
        {page > 4 && (
          <button
            className={`${classes.nextBtn} ${page > 4 ? classes.hoverBtn : ""}`}
          >
            {page}
          </button>
        )}
        {count > 6 && <span>...</span>}
      </div>
      <button className={classes.nextBtn} onClick={nextBtn}>{`>`}</button>
    </div>
  );
};
export default memo(Pagination);
