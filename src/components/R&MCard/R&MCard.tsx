import React from "react";
import classes from "./R&M.module.scss";
import Card from "../card/Card";
import { useSelector } from "react-redux";
import { ReduxTypes } from "../../store/types";
import { ResultTypes } from "./R&MTypes";

const RMCard=()=>{
    const RAndMInfo = useSelector(
        (state:ReduxTypes) => state.RAndMData.characters
    );
return(
 <>
   {RAndMInfo?.results?.map((item:ResultTypes)=>
    <Card key={item.id} className={classes.RMContainer}>
      <div className={classes.descriptions}>
        <p><span>name :</span> {item.name}</p>
        <p><span>status :</span> {item.status}</p>
        <p><span>species :</span> {item.species}</p>
        <p><span>type :</span> {item.type}</p>
        <p><span>created :</span> {item?.created.split("T")[0]}</p>
        <p><span>gender :</span> {item.gender}</p>
      </div>
      <img src={item.image} alt="" className={classes.img} loading="lazy" />
    </Card> 
    )}
 </>
)
}
export default RMCard
