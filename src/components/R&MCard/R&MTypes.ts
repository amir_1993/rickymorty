export interface RMTypes{
characters:{
    info:{
        pages:number;
    },
    results:ResultTypes[]
  }
}

export interface ResultTypes{
    name:string;
    id:number;
    status:string;
    type:string;
    species:string
    image:string
    created:string
    gender:string
}