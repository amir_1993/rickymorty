import React from "react";
import { card } from "./types";
import classes from './card.module.scss'

const Card=({children,className}:card)=>{
    return(
      <div className={`${classes.card_container} ${className?className:""}`}>
       {children}
      </div>
    )
}
export default Card;