import React from 'react';
import { ApolloProvider, ApolloClient, InMemoryCache } from "@apollo/client";
import Products from './pages/products/Products';
import { Provider } from "react-redux";
import store from "./store";
import classes from './app.module.scss'

const client = new ApolloClient({
  uri: process.env.REACT_APP_BASE_URL,
  cache: new InMemoryCache()
});

function App() {
  return (
  <Provider store={store}>
   <ApolloProvider client={client}>
    <div className={classes.container}>
    <Products/>
    </div>
   </ApolloProvider>
  </Provider>
  );
}

export default App;
