import { configureStore } from "@reduxjs/toolkit";
import RAndM from "./products/products";

const state = configureStore({
  reducer: {
    RAndMData: RAndM.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export default state;
